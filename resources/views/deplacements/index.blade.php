@extends('layouts.app')

@section('content')
 <title>deplacement</title>
 <h1>c'est la page index</h1>
  @if(count($deplacements)> 1)
   <div class="well">
  <table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">NOM & PRÉNOM</th>
            <th scope="col">DATE DÉPART</th>
            <th scope="col">HEURE DÉPART</th>
            <th scope="col">DATE RETOUR</th>
            <th scope="col">HEURE RETOUR</th>
            <th scope="col">VILLE</th>
            <th scope="col">MOYEN</th>
            <th scope="col"></th>
            <th scope="col"></th>
         </tr>
    </thead>
    <tbody>

      @foreach($deplacements as $deplacement)
     <tr>
            <td>{{ $deplacement->nom }}</td>
            <td>{{ $deplacement->date_de_départ }}</td>
            <td>{{ $deplacement->heure_de_départ}}</td>
            <td>{{ $deplacement->date_de_retour }}</td>
            <td>{{ $deplacement->heure_de_retour}}</td>
            <td>{{ $deplacement->idVille}}</td>
            <td>{{ $deplacement->idMoyen}}</td>
            <td><a href="/deplacements/{{$deplacement->id}}/edit" class="btn btn-outline-primary">modifier<a><td>
                       <td>{!! Form::open(['action' => ['depcontroller@destroy', $deplacement->id], 'method'=>'POST']) !!}
                   {{Form::hidden('_method', 'DELETE')}}
                   {{Form::submit('supprimer', ['class' =>'btn btn-outline-danger'])}}
                  {!! Form::close()!!}
                       <td>
        </tr> 
   
               <h3>{{$deplacement->title}}</h3>
            </div>
      @endforeach
    </table>  
  @else
         <p>pas de deplacements jusqu'à maintenant</p>          
              
  @endif 
@endsection