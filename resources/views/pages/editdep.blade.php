@extends('layouts.app')

@section('content')
<br/>
    <h1>Modifier un déplacement</h1>
    <div>
    <div class= 'col-md-6'>
    
        {{ Form::open(['action' => ['depcontroller@update', $deplacements->id]]) }}
            <div class="form-groupe">
                {{Form::label('nom', 'Nom')}}
                {{Form::text('nom',  $deplacements->nom, ['class' => 'form-control', 'placeholder' => 'Nom'])}}
            </div>
            
            <div class="form-groupe">
                {{Form::label('prenom', 'Prénom')}}
                {{Form::text('prenom', $deplacements->prenom, ['class' => 'form-control', 'placeholder' => 'Prénom'])}}
            </div >
            <div class= 'form-group row'>
            <div class ='col-sm-5'>
            
                {{Form::label('date_de_départ', 'Date de départ')}}
                {{Form::date('date_de_départ',  $deplacements->date_de_départ, ['class' => 'form-control'])}}
            
            </div>
            
            <div class ='col-sm-5'>
            
                {{Form::label('heure_de_départ', 'Heure de départ')}}
                {{Form::time('heure_de_départ',  $deplacements->heure_de_départ, ['class' => 'form-control'])}}
        
            </div>
            </div>
            
            <div class= 'form-group row'>
            <div class ='col-sm-5'>
        
                {{Form::label('date_de_retour', 'Date de retour')}}
                {{Form::date('date_de_retour',  $deplacements->date_de_retour, ['class' => 'form-control'])}}
            </div>
            
            <div class ='col-sm-5'>
            
                {{Form::label('heure_de_départ', 'Heure de départ')}}
                {{Form::time('heure_de_départ',  $deplacements->heure_de_départ, ['class' => 'form-control'])}}
        
            </div>
            </div>
            
            
            <div class= 'form-group row'>
            <div class ='col-sm-5'>
            
                {{Form::label('ville', 'Ville')}}
                {{Form::select('ville', $villesDropDown , ['class' => 'form-control'])}}
            </div>
            
            
            <div class ='col-sm-5'>
                {{Form::label('moyen', 'Moyen')}}
                {{Form::select('moyen', $moyensDropDown , ['class' => 'form-control'])}}
            </div>
            
            </div>
            <br/>
            {{form::hidden('_method', 'PUT')}}
            {{Form::submit('Ajouter', ['class' => 'btn btn-primary',])}}
        {{ Form::close() }}
        
        
        </div>
        </div>
@endsection   