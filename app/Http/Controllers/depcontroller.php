<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\deplacement;
use App\ville;
use App\moyen;


class depcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deplacements = deplacement::all();
       return view('deplacements.index')->with('deplacements', $deplacements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $villesDropDown = ville::pluck('nomville', 'id');
        $moyensDropDown = moyen::pluck('nommoyen', 'id');
        return view('pages.ajoutdep', compact('moyensDropDown'), compact('villesDropDown'));
        return view('pages.ajoutdep');  
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { $this -> validate($request, [
        'nom' => 'required'
    ]);
    
    // Create deplacement
    $deplacements = new deplacement;
    $deplacements -> nom = $request -> input('nom');
    $deplacements -> date_de_départ = $request -> input('date_de_départ');
    $deplacements -> heure_de_départ = $request -> input('heure_de_départ');
    $deplacements -> date_de_retour = $request -> input('date_de_retour');
    $deplacements -> heure_de_retour = $request -> input('heure_de_retour');
    $deplacements -> idVille = $request -> input('idVille');
    $deplacements -> idMoyen = $request -> input('idMoyen');
    $deplacements -> save();
    
    return redirect('/')->with ('success', 'Déplacement créé');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          
        $deplacements = deplacement::find($id);
      // return view('deplacements.editdep')
       //select box
       $villesDropDown = ville::pluck('nomville', 'id');
       $moyensDropDown = moyen::pluck('nommoyen', 'id');
       return view('deplacements.editdep', compact('moyensDropDown'), compact('villesDropDown'))->with('deplacements', $deplacements);
       return view('deplacements.editdep');    

       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this -> validate($request, [
            'nom' => 'required'
        ]);
        
        // update deplacement
        $deplacements = deplacement::find($id);
        $deplacements -> nom = $request -> input('nom');
        $deplacements -> date_de_départ = $request -> input('date_de_départ');
        $deplacements -> heure_de_départ = $request -> input('heure_de_départ');
        $deplacements -> date_de_retour = $request -> input('date_de_retour');
        $deplacements -> heure_de_retour = $request -> input('heure_de_retour');
        $deplacements -> idVille = $request -> input('idVille');
        $deplacements -> idMoyen = $request -> input('idMoyen');
        $deplacements -> save();
        
         
       
        
        return redirect('/')->with('success', 'Déplacement modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deplacements = deplacement::find($id);
        $deplacements->delete();
        return redirect('/')->with('success', 'Déplacement supprimé');

    }
}
