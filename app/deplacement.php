<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deplacement extends Model
{
    // Table Name 
    protected $table = 'deplacements';
    // Primary Key
    public $primaryKey = 'id';

    //relationship
    public function villes(){

        return $this->hasOne('App\ville');
    }
}
