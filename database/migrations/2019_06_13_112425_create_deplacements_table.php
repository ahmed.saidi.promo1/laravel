<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deplacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->date('date_de_départ');
            $table->time('heure_de_départ');
            $table->date('date_de_retour');
            $table->time('heure_de_retour');
            $table->integer('idVille');
            $table->integer('idMoyen');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deplacements');
    }
}
